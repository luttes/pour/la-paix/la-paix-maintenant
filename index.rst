
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


.. un·e
.. ❤️💛💚

|FluxWeb| `RSS <https://luttes.frama.io/pour/la-paix/la-paix-maintenant/rss.xml>`_


.. _paix_maintenant:
.. _lpm:

===============================================================================
|lpm| **La Paix maintenant (LPM)**
===============================================================================


- https://rstockm.github.io/mastowall/?hashtags=paix,peace,pace&server=https://framapiaf.org

.. toctree::
   :maxdepth: 6

   militantes/militantes
